package com.minsky.algirthms.leetcode;

public class ListNode {
	int val;
	ListNode next;
	public ListNode(int val) {
		this.val = val;
	}
}
