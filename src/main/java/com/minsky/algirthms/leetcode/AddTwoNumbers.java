package com.minsky.algirthms.leetcode;
/*
 * You are given two linked lists representing two non-negative numbers. 
 * The digits are stored in reverse order and each of their nodes contain a single digit. 
 * Add the two numbers and return it as a linked list.
 * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 * Output: 7 -> 0 -> 8
 * 
 * You may describe this problem as add two non-negative numbers,
 * the different thing is you should present it in ListNode.
 */
public class AddTwoNumbers {
	public static void main(String[] args) {
		
	}
	/**
	 * This method if for add two non-negative numbers,
	 * Runtime: 4 ms
	 * my runtime beats 35.18% of javasubmissions.
	 * @param l1
	 * @param l2
	 * @return
	 */
	public ListNode addTwoNumbers(ListNode l1,ListNode l2){
		ListNode head,node;
		head = node = new ListNode(0);
		int carry =0;
		while(l1!=null||l2!=null||carry!=0){
			int sum = (l1!=null?l1.val:0)+(l2!=null?l2.val:0)+carry;
			node.val = sum%10;
			carry = sum/10;
			node = node.next = new ListNode(sum%10);
			l1 =(l1!=null ?l1.next:null);
			l2 =(l2!=null ?l2.next:null);
		}
		return head.next;
	}
}
