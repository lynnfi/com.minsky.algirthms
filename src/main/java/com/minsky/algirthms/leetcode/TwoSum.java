package com.minsky.algirthms.leetcode;
/*
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * You may assume that each input would have exactly one solution.
 * Example:
 * Given nums = [2, 7, 11, 15], target = 9,
 * Because nums[0] + nums[1] = 2 + 7 = 9,
 * return [0, 1].
 */

import java.util.HashMap;
import java.util.Map;


public class TwoSum {
	public static void main(String[] args) {
		TwoSum testTwoSum = new TwoSum();
		int[] nums = {1,2,3,4,5};
		int target = 3;
		int[] result = testTwoSum.twoSum(nums, target);
		System.out.println("Result is:"+result[0]+"-----"+result[1]);
	}
/**
 * use this method to find which two numbers add will equals target;
 * you should assume your input would have exactly one solution. 
 * @param nums
 * @param target
 * @return
 */
	public int[] twoSum(int[] nums,int target) {
		int[] result = new int[2];
		Map<Integer, Integer> map = new HashMap<Integer,Integer>();
		for(int i = 0; i<nums.length;i++){
			if(map.containsKey(nums[i]))
			{
				result[0]=map.get(nums[i]);
				result[1]=i;
				break;
			}
			else{
				map.put(target-nums[i],i);
			}
		}
		
		return result;
	}
}
